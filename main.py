import pyglet
from pyglet.window import key
from pyglet.window import mouse
import argparse
import os
import sys
import _thread
from src import network
from src import game

# -----------------------------------------------------------------------------
arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('--online',
                        type=str,
                        help='Set the game to play online. Either host or client.',
                        choices=['host', 'client'],
                        default=None)
arg_parser.add_argument('--address',
                        type=str,
                        help='Set the address to connect to if playing online',
                        default=None)
arg_parser.add_argument('--port',
                        type=int,
                        help='Set the port to connect to if playing online.',
                        default=None)
arg_parser.add_argument('--ai',
                        action='store_true',
                        help='Set if you want to play against simple AI.',
                        default=None)



# -----------------------------------------------------------------------------
# define main objects
board = pyglet.resource.image(os.path.join('images', 'board.png'))
pawn_white    = pyglet.resource.image(os.path.join('images', 'pawn_white.png'))
pawn_black    = pyglet.resource.image(os.path.join('images', 'pawn_black.png'))
rook_white    = pyglet.resource.image(os.path.join('images', 'rook_white.png'))
rook_black    = pyglet.resource.image(os.path.join('images', 'rook_black.png'))
bishop_white  = pyglet.resource.image(os.path.join('images', 'bishop_white.png'))
bishop_black  = pyglet.resource.image(os.path.join('images', 'bishop_black.png'))
knight_white  = pyglet.resource.image(os.path.join('images', 'knight_white.png'))
knight_black  = pyglet.resource.image(os.path.join('images', 'knight_black.png'))
queen_white   = pyglet.resource.image(os.path.join('images', 'queen_white.png'))
queen_black   = pyglet.resource.image(os.path.join('images', 'queen_black.png'))
king_white    = pyglet.resource.image(os.path.join('images', 'king_white.png'))
king_black    = pyglet.resource.image(os.path.join('images', 'king_black.png'))
turn_you      = pyglet.resource.image(os.path.join('images', 'turn_you.png'))
turn_enemy    = pyglet.resource.image(os.path.join('images', 'turn_enemy.png'))
sel_field     = pyglet.resource.image(os.path.join('images', 'selected.png'))
menu_bg       = pyglet.resource.image(os.path.join('images', 'menu_bg.png'))
new_game_menu = pyglet.resource.image(os.path.join('images', 'new_game.png'))
hints_img     = pyglet.resource.image(os.path.join('images', 'hints.png'))
rollback_img  = pyglet.resource.image(os.path.join('images', 'rollback.png'))

# -----------------------------------------------------------------------------
# define sounds
click_sound = pyglet.resource.media(
                os.path.join('sounds', 'click.mp3'),
                streaming=False
)
error_sound = pyglet.resource.media(
                os.path.join('sounds', 'error_sound.mp3'),
                streaming=False
)
end_sound = pyglet.resource.media(
                os.path.join('sounds', 'end_sound.mp3'),
                streaming=False
)


# -----------------------------------------------------------------------------
# define figures
figures = {
    ('p', 'white'): pawn_white,
    ('R', 'white'): rook_white,
    ('B', 'white'): bishop_white,
    ('k', 'white'): knight_white,
    ('Q', 'white'): queen_white,
    ('K', 'white'): king_white,
    ('p', 'black'): pawn_black,
    ('R', 'black'): rook_black,
    ('B', 'black'): bishop_black,
    ('k', 'black'): knight_black,
    ('Q', 'black'): queen_black,
    ('K', 'black'): king_black
}


# -----------------------------------------------------------------------------
# Create window, allow transpacencym, and create game
window = pyglet.window.Window(1000, 800)
pyglet.gl.glEnable(pyglet.gl.GL_BLEND)
pyglet.gl.glBlendFunc(pyglet.gl.GL_SRC_ALPHA, pyglet.gl.GL_ONE_MINUS_SRC_ALPHA)
g = game.Game()
g.init()


# -----------------------------------------------------------------------------
# create winning frames
black_won = pyglet.text.Label('BLACK WON',
                          font_name='Times New Roman',
                          font_size=60,
                          color = (0, 0, 0, 255),
                          x=window.width//2, y=window.height//2,
                          anchor_x='center', anchor_y='center')


white_won = pyglet.text.Label('WHITE WON',
                          font_name='Times New Roman',
                          font_size=60,
                          color = (0, 0, 0, 255),
                          x=window.width//2, y=window.height//2,
                          anchor_x='center', anchor_y='center')

new_game_opt = pyglet.text.Label('New Game',
                          font_name='Times New Roman',
                          font_size=60,
                          x=window.width-200, y=window.height-50,
                          anchor_x='center', anchor_y='center')


# -----------------------------------------------------------------------------
def handle_menu(x, y, button, modifiers):
    if y > 630:
        if g.is_online == False:
            g.restart()
        else:
            print('Can\'t restart an online game. Setup a new session.')
    elif 585 > y > 550:
        g.show_hints = not g.show_hints
        click_sound.play()
    elif 545 > y > 510 and g.playing:
        if len(g.prev_states) != 0:
            g.go_back()
            g.selected = False
            print('Rolling back...')


# -----------------------------------------------------------------------------
def draw_menu():
    menu_bg.blit(0, 0)
    new_game_menu.blit(0, 630)
    hints_img.blit(0, 550)
    if len(g.prev_states) != 0 and not g.is_online:
        rollback_img.blit(0, 475)


# -----------------------------------------------------------------------------
def coor_to_field(x, y):
    x -= 200
    one_8 = board.width//8
    new_x = x//one_8
    new_y = 7-(y//one_8)
    return new_x, new_y


# -----------------------------------------------------------------------------
@window.event
def on_key_press(symbol, modifiers):
    if symbol == key.A:
        print('The "A" key was pressed.')
    elif symbol == key.LEFT:
        print('The left arrow key was pressed.')
    elif symbol == key.ENTER:
        print('The enter key was pressed.')


# -----------------------------------------------------------------------------
@window.event
def on_draw():
    board.blit(200, 0)
    draw_menu()
    # display who's turn is it only if playing online
    if g.is_online:
        if g.turn == g.player_color:
            turn_you.blit(20,300)
        else:
            turn_enemy.blit(20, 300)
    # print all the pieces
    for r in range(8):
        for c in range(8):
            if g.field[r, c] != None:
                fig = g.field[r, c].ch
                color = g.field[r, c].color
                figures[fig, color].blit(200+c*100, 700-r*100)
    if g.playing == False:
        if g.winner == 'white':
            white_won.draw()
        else:
            black_won.draw()
    # draw selected piece options
    if g.selected:
        char = g.field[g.selected_coords].ch
        color = g.field[g.selected_coords].color
        img = figures[(char, color)]
        img.width = 120
        img.height = 120
        img.blit(190+g.selected_coords[1]*100, 690-g.selected_coords[0]*100)
        img.width = 100
        img.height = 100
        
        if g.show_hints:
            for y, x in g.possible_moves_coords:
                x_place = 230+x*100
                y_place = 730-y*100
                sel_field.blit(x_place, y_place)


# -----------------------------------------------------------------------------
@window.event
def on_mouse_press(x, y, button, modifiers):
    if x < 200:
        handle_menu(x, y, button, modifiers)
        return
    if g.playing == False:
        return
    if g.turn != g.player_color and g.is_online:
        print('Not your turn!')
        error_sound.play()
        return
    if button == mouse.LEFT:
        # convert coordinates to position on map
        x, y = coor_to_field(x, y)
        if g.selected:
            if g.selected_coords == (y, x):
                g.selected = False
                return
            res = g.move(g.selected_coords, (y, x))
            # res is true if move was successful
            if res == True:
                click_sound.play()
                if g.is_online:
                    g.response = str(g.selected_coords)+str((y, x))
                # play end sound if ended
                if not g.playing:
                    end_sound.play()
                if g.is_AI == True and g.playing:
                    ai_move = g.get_ai_move()
                    g.move(*ai_move)
            else:
                error_sound.play()
            g.selected = False
        else:
            if g.field[y, x] == None:
                return
            if g.field[y, x].color != g.turn:
                print('Not your turn!')
                error_sound.play()
                return
            g.selected_coords = y, x
            g.selected = True
            g.possible_moves_coords = g.generate_possible_moves(g.selected_coords)


# -----------------------------------------------------------------------------
if __name__ == '__main__':      
    args = arg_parser.parse_args()
    # we can define a click here if we are host of playing locally
    if args.online == None:
        print('We are playing locally.')
        if args.ai:
            print('Playing against my super smart AI :)')
        g.is_AI = args.ai
        g.is_online = False
        g.player_color = 'white'
    else:
        g.is_online = True
        # we play online :)
        addr = args.address
        port = args.port
        role = args.online
        print(f'We are playing online via: {addr} at {port} as {role}')
        if role == 'host':
            g.player_color = 'black'
            player_network = network.Server(addr, port, g)
            # define waiting loop
            def check_input(dt):
                # if it's my turn, i can go
                if g.turn == 'black':
                    return
                # else wait for a move from the network :)
                _thread.start_new_thread(player_network.move, ())
        else:           
            g.player_color = 'white'
            player_network = network.Client(addr, port, g)
            def check_input(dt):
                # checking if we had made a move yet or not
                if g.response == None:
                    return
                on_draw()
                resp = []
                for i in [1, 4, 7, 10]:
                    resp.append(int(g.response[i]))
                player_response = player_network.move(resp)
                print('Got response :', player_response)
                if player_response == '':
                    print('Connection lost...')
                    sys.exit()
                g.response = None
                resp = []
                for i in [1, 4, 7, 10]:
                    resp.append(int(player_response[i]))
                g.move((resp[0], resp[1]), (resp[2], resp[3]))
                on_draw()

        pyglet.clock.schedule_interval(check_input, .5)
        
    pyglet.app.run()
    print('Window closed, terminating...')
