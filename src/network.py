import socket
import _thread
import threading
import sys
import time


# =============================================================================
class Server:
    def __init__(self, server, port, gm):
        self.server = server
        self.port = port
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.frm = None
        self.to = None
        self.try_establish()
        self.gm = gm
    
    def try_establish(self):
        # try use socket
        try:
            self.socket.bind((self.server, self.port))
        except socket.error as e:
            print('Couldn\'t start server.')
            print(str(e))
            sys.exit()
        # We have been succesfull
        self.socket.listen(2)
        print('Waiting for a connection...')
    
    def get_threaded_connection_input(self, conn):
        conn.send(str.encode('Connected...'))
        reply = ''
        while True:
            try:
                data = conn.recv(2048)
                reply = data.decode('utf-8')
                if not data:
                    print('Disconnected')
                    break
                frm = int(reply[1]), int(reply[4])
                to = int(reply[7]), int(reply[10])
                self.gm.move(frm, to)
                while self.gm.response == None:
                    time.sleep(0.5)
                # here we send a response back
                conn.sendall(str.encode(self.gm.response))
                self.gm.response = None
            except:
                break
        print('Connection closed...')
        conn.close()
        
    def move(self):
        self.cnn = None
        for i in range(2):
            conn, addr = self.socket.accept()
            print(f'Connected to: {addr}')
            if i == 1:
                break
            _thread.start_new_thread(self.get_threaded_connection_input, (conn, ))


# =============================================================================
class Client:
    def __init__(self, server, port, gm):
        self.client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server = server
        self.port = port
        self.addr = (self.server, self.port)
        self.id = self.connect()
        print(self.id)

    def connect(self):
        try:
            self.client.connect(self.addr)
            return self.client.recv(2048).decode()
        except ConnectionRefusedError as e:
            print('Couldn\'t connect from client.')
            print(e)
            sys.exit()

    def move(self, coords):
        try:
            msg = []
            for i in coords:
                msg.append(i)
            msg = str(msg)
            # we send a message here
            self.client.send(str.encode(msg))
            # we wait to recieve a message, then return it
            return self.client.recv(2048).decode()
        except socket.error as e:
            print(e)
