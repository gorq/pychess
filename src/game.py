import numpy as np
import copy
import random
import _thread

# =============================================================================
class Figure:
    def __init__(self):
        self.value = 0
        self.ch = '_'
        self.color = None
        self.starting = True

    def __repr__(self):
        return self.ch

    def can_go_to(self, x, y, taken, field, just_checking):
        return False

    def see_possible_moves(self, frm, field):
        res = []
        for y in range(8):
            for x in range(8):
                taken = field[y, x] is not None
                if taken:
                    if field[y, x].color == self.color:
                        continue
                if self.can_go_to(frm, (y, x), taken, field, True):
                    res.append((y, x))
        return res

# =============================================================================
class Pawn(Figure):
    def __init__(self, color):
        super().__init__()
        self.value = 10 if color == 'white' else -10
        self.ch = 'p'
        self.color = color

    def can_go_to(self, frm, to, taken, field, just_checking):
        # if given field is taken
        if taken:
            if self.color == 'white':
                return abs(frm[1]-to[1]) == 1 and frm[0]-to[0] == 1
            else:
                return abs(to[1]-frm[1]) == 1 and to[0]-frm[0] == 1
            
        if self.color == 'white':
            if self.starting:
                return frm[1] == to[1] and 0 < frm[0]-to[0] <= 2
            else:
                return frm[1] == to[1] and frm[0]-to[0] == 1
        else:
            if self.starting:
                return frm[1] == to[1] and 0 < to[0]-frm[0] <= 2
            else:
                return frm[1] == to[1] and to[0]-frm[0] == 1
    

        
# =============================================================================
class Knight(Figure):
    def __init__(self, color):
        super().__init__()
        self.value = 30 if color == 'white' else -30
        self.ch = 'k'
        self.color = color

    def can_go_to(self, frm, to, taken, field, just_checking):
        if abs(frm[0]-to[0]) == 1:
            return abs(frm[1]-to[1]) == 2
        elif abs(frm[0]-to[0]) == 2:
            return abs(frm[1]-to[1]) == 1
        else:
            return False


# =============================================================================
class Bishop(Figure):
    def __init__(self, color):
        super().__init__()
        self.value = 30 if color == 'white' else -30
        self.ch = 'B'
        self.color = color
        
    def can_go_to(self, frm, to, taken, field, just_checking):
        # asser that we are moving diagonally
        if abs(frm[0]-to[0]) != abs(frm[1]-to[1]):
            return False
        # else check all the fields and see if we don't jump through something
        y_change = 1 if frm[0] < to[0] else -1
        x_change = 1 if frm[1] < to[1] else -1
        y_tmp = frm[0]+y_change
        x_tmp = frm[1]+x_change
        while True:
            if y_tmp == to[0]:
                break
            if field[y_tmp, x_tmp] != None:
                return False
            x_tmp += x_change
            y_tmp += y_change
        return True


# =============================================================================
class Rook(Figure):
    def __init__(self, color):
        super().__init__()
        self.value = 50 if color == 'white' else -50
        self.ch = 'R'
        self.color = color
    
    def can_go_to(self, frm, to, taken, field, just_checking):
        if frm[0] == to[0]: # going horizontally
            change = (0, 1) if frm[1] < to[1] else (0, -1)
        elif frm[1] == to[1]: # going vertically
            change = (1, 0) if frm[0] < to[0] else (-1, 0)
        else:
            return False
        y_tmp = frm[0]+change[0]
        x_tmp = frm[1]+change[1]
        while x_tmp >= 0 and x_tmp < 8 and y_tmp >= 0 and y_tmp < 8:
            if y_tmp == to[0] and x_tmp == to[1]:
                break
            if field[y_tmp, x_tmp] != None:
                return False
            y_tmp += change[0]
            x_tmp += change[1]
        return True


# =============================================================================
class Queen(Figure):
    def __init__(self, color):
        super().__init__()
        self.value = 90 if color == 'white' else -90
        self.ch = 'Q'
        self.color = color
        self.rook = Rook(color)
        self.bishop = Bishop(color)
    
    def can_go_to(self, frm, to, taken, field, just_checking):
        if frm[0] == to[0] or frm[1] == to[1]:
            return self.rook.can_go_to(frm, to, taken, field, just_checking)
        else:
            return self.bishop.can_go_to(frm, to, taken, field, just_checking)


# =============================================================================
class King(Figure):
    def __init__(self, color):
        super().__init__()
        self.value = 900 if color == 'white' else -900
        self.ch = 'K'
        self.color = color
    
    def check_castle(self, frm, to, taken, field, just_checking):
        # going to left
        rook_pos = None
        if to[1] == 1:
            rook_pos = 0, 0
        elif to[1] == 2:
            rook_pos = 7, 0
        elif to[1] == 5:
            rook_pos = 0, 7
        else:
            rook_pos = 7, 7
        if (
            field[rook_pos] is not None and
            field[rook_pos].ch == 'R' and
            field[rook_pos].color == self.color
        ):
            # check the fields between:
            change = -1 if to[1] in [5, 6] else 1
            tmp_ch = rook_pos[1]+change
            while tmp_ch != frm[1]:
                if field[frm[0], tmp_ch] is not None:
                    return False
                tmp_ch += change
            # we passed, move rook, and return True
            if not just_checking:
                field[frm[0], rook_pos[1]].starting = False
                field[frm[0], tmp_ch-change] = field[frm[0], rook_pos[1]]
                field[frm[0], rook_pos[1]] = None
            return True
            
        return False
                
    def can_go_to(self, frm, to, taken, field, just_checking):
        if self.starting and frm[0] == to[0] and abs(frm[1]-to[1]) == 2:
            return self.check_castle(frm, to, taken, field, just_checking)
        return abs(frm[0]-to[0]) <= 1 and abs(frm[1]-to[1]) <= 1


# =============================================================================
class Move:
    def __init__(self, frm, to, parent):
        self.frm = frm
        self.to = to
        self.parent = parent


# =============================================================================
class Game:
    def __init__(self, is_online=False):
        self.field = np.array([[None]*8]*8)
        self.selected = False
        self.selected_coords = None
        self.turn = 'white'
        self.playing = True
        self.winner = None
        self.is_online = is_online
        self.response = None
        self.player_color = None
        self.possible_moves_coords = []
        self.show_hints = False
        self.prev_states = []
        self.is_AI = False
        
    def generate_possible_moves(self, coords):
        return self.field[coords].see_possible_moves(coords, self.field)
    
    def go_back(self):
        self.field = self.prev_states[-1]
        self.prev_states.pop()
        self.turn = 'white' if self.turn == 'black' else 'black'
        if self.is_AI:
            self.field = self.prev_states[-1]
            self.prev_states.pop()
            self.turn = 'white'
    
    def get_value(self):
        res = 0
        for y in range(8):
            for x in range(8):
                if self.field[y, x] is not None:
                    res += self.field[y, x].value
        return res

    def init(self):
        # place pawns
        for i in range(8):
            self.field[6, i] = Pawn('white')
            self.field[1, i] = Pawn('black')
        # place rooks
        for i in [0, 7]:
            self.field[7, i] = Rook('white')
            self.field[0, i] = Rook('black')
        # place horses
        for i in [1, 6]:
            self.field[7, i] = Knight('white')
            self.field[0, i] = Knight('black')
        # place bishops
        for i in [2, 5]:
            self.field[7, i] = Bishop('white')
            self.field[0, i] = Bishop('black')
        # place queens
        self.field[7, 3] = Queen('white')
        self.field[0, 3] = Queen('black')
        # place kings
        self.field[7, 4] = King('white')
        self.field[0, 4] = King('black')
    
    def restart(self):
        self.__init__()
        self.init()        
    
    def get_ai_move(self):
        moves = []
        current_value = None
        # for each figure
        for y in range(8):
            for x in range(8):
                if self.field[y, x] is None or self.field[y, x].color == 'white':
                    continue
                # get all possible moves
                for mv in self.generate_possible_moves((y, x)):
                    tmp_game = Game()
                    tmp_game.field = copy.deepcopy(self.field)
                    tmp_game.turn = 'black'
                    tmp_game.move((y, x), mv)
                    tmp_val = tmp_game.get_value()
                    if moves == []:
                        moves.append(((y, x), mv))
                        current_value = tmp_val
                    elif tmp_val < current_value:
                        moves = [((y, x), mv)]
                        current_value = tmp_val
                    elif tmp_val == current_value:
                        moves.append(((y, x), mv))
        return moves[random.randint(0, len(moves)-1)]


    def move(self, frm, to):
        if self.field[frm] != None and self.field[frm].color != self.turn:
            print('Not your figure')
            return False
        taken = self.field[to] is not None
        if taken == True:
            if self.field[frm].color == self.field[to].color:
                print('Trying to take same player figure, illegal')
                return False
        # Check if given figure is indeed able to walk to chosen tile
        if self.field[frm].can_go_to(frm, to, taken, self.field, False):
            self.prev_states.append(copy.deepcopy(self.field))
            if taken and self.field[to].ch == 'K':
                self.playing = False
                self.winner = self.field[frm].color
            self.field[to] = self.field[frm]
            self.field[to].starting = False
            self.field[frm] = None
            # promot pawn to queen if reached end
            if self.field[to].ch == 'p':
                if self.field[to].color == 'black' and to[0] == 7:
                    self.field[to] = Queen('black')
                elif self.field[to].color == 'white' and to[0] == 0:
                    self.field[to] = Queen('white')
            self.turn = 'white' if self.turn == 'black' else 'black'
            return True
        else:
            print('Illegal move')
            return False
